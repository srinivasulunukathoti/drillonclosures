 // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
function cacheFunction(callBack) {
    //create onject to store
    const object = {};
    return (name)=>{
        // check data present or not
        if (Object.keys(object).includes(name)) {
            console.log(object[name]);
        }
        else{
            //calling callback function
            const result = callBack(name);
            object[name] = result;
            console.log(result);
            
        }
       
    }
};
//export the code
module.exports = cacheFunction;


