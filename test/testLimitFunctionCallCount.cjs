const limitFunctionCallCount = require('../limitFunctionCallCount.cjs')

function callBack(count) {
    console.log("Callback invoked", count);
}

try {
    const count = limitFunctionCallCount(callBack , 3);
    count();
    count();
    count();
    count();
    count();
} catch (error) {
    console.log(error);
}
