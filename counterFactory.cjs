 // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.

function counterFactory() {
    // variable declaration with initilization
    let count = 0;

    return {
        // this function will return increment count
        increment : function () {
             count++;
             return count;
        },
        // this function will return decrement count
       
        decrement : function () {
             count--;
             return count;
        }
    }

};
//export the code
module.exports = counterFactory;