 // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(callBack , number) {
    // variable declaration with initilization
    let count = 0;
    return ()=>{
        // check the condition
        if (count < number) {
            count++;
            // calling the callback funtion and returning.
            callBack(count);
        } else {
            return null;
        }
    }
};
//expotrs the code
module.exports = limitFunctionCallCount;